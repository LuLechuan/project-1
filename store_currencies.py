import pandas as pd
import datetime
import requests
import sqlite3
import configparser
from threading import Lock
from boundedExecutor import BoundedExecutor

CONFIG = configparser.ConfigParser()
CONFIG.read('common.ini')

DOMAIN = CONFIG['keys']['domain']
ACCESS_KEY = CONFIG['keys']['access_key']

conn = sqlite3.connect(CONFIG['keys']['db'])
times = pd.date_range(start=CONFIG['keys']['start_date'], end=CONFIG['keys']['end_date'], freq='D')
executor = BoundedExecutor(5, 5)
lock = Lock()

def get_info_from_one_time(time):
    formatted_time = str(time).split(" ")[0]
    url = "{}/{}?access_key={}&format=1".format(DOMAIN, formatted_time, ACCESS_KEY)
    r = requests.get(url=url)
    data = r.json()
    rates = data['rates']
    df = pd.DataFrame(list(rates.items()))
    df['Date'] = time
    df.columns = ['Code', 'Rate', 'Date']
    lock.acquire()
    df.to_sql("ExchangeRate", conn, if_exists="append")
    lock.release()
    print("DONE with {}".format(formatted_time))

for time in times:
    executor.submit(get_info_from_one_time(time))
