import pandas as pd
from os import listdir
from os.path import isfile, join
from threading import Lock
import argparse
import sqlite3
import configparser

from boundedExecutor import BoundedExecutor

CONFIG = configparser.ConfigParser()
CONFIG.read('common.ini')

RAW_DATA_FOLDER = CONFIG["keys"]["raw_data_folder"]
EXPORT_PATH = CONFIG["keys"]["export_path"]

executor = BoundedExecutor(5, 5)
import_files = [f for f in listdir(RAW_DATA_FOLDER) if isfile(join(RAW_DATA_FOLDER, f))]
require_header = True
conn = sqlite3.connect(CONFIG['keys']['db'])
lock = Lock()
parser = argparse.ArgumentParser(description='Decide to save as sql or not')
parser.add_argument('use_sqlite', type=bool,
                    help='decide to save as sql or not')
args = parser.parse_args()
use_sqlite = args.use_sqlite

print(import_files)

def store_to_csv():
    with open(EXPORT_PATH, 'a') as out:
        for import_file in import_files:
            executor.submit(simple_work_to_csv(import_file, out))

def store_to_sql():
    for import_file in import_files:
        executor.submit(simple_work_to_sql(import_file))

def simple_work_to_csv(import_file, out):
    filepath = "./{}/{}".format(RAW_DATA_FOLDER, import_file)
    df = pd.read_csv(filepath, header=0)
    df['Code'] = import_file.split(".")[0]
    global require_header
    if require_header:
        lock.acquire()
        df.to_csv(out, index=False)
        require_header = False
        lock.release()
    else:
        lock.acquire()
        df.to_csv(out, header=require_header, index=False)
        lock.release()

def simple_work_to_sql(import_file):
    filepath = "./{}/{}".format(RAW_DATA_FOLDER, import_file)
    df = pd.read_csv(filepath, header=0)
    df['Code'] = import_file.split(".")[0]
    lock.acquire()
    df.to_sql("stocks", conn, if_exists="append")
    lock.release

if use_sqlite:
    store_to_sql()
else:
    store_to_csv()
